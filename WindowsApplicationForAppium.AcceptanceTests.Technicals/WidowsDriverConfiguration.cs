﻿using System;
using System.IO;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Windows;

namespace WindowsApplicationForAppium.AcceptanceTests.Technicals
{
    internal static class WidowsDriverConfiguration
    {
        private const string WindowsApplicationDriverUrl = "http://127.0.0.1:4723";
        private const string appExecutableFilePath = @"..\..\..\WindowsApplicationForAppium\bin\Debug\WindowsApplicationForAppium.exe";

        public static WindowsDriver<WindowsElement> WindowsDriver { get; private set; }

        public static void Setup()
        {
            if (!File.Exists(appExecutableFilePath))
                throw new ApplicationException($"{appExecutableFilePath} is not exist.");

            var appExecutableFileFullName = new FileInfo(appExecutableFilePath).FullName;

            if (WindowsDriver != null)
                return;

            try
            {
                var appiumOptions = new AppiumOptions();
                appiumOptions.AddAdditionalCapability("app", appExecutableFileFullName);
                appiumOptions.AddAdditionalCapability("deviceName", "WindowsPC");
                WindowsDriver = new WindowsDriver<WindowsElement>(new Uri(WindowsApplicationDriverUrl), appiumOptions);
            }
            catch (Exception exception)
            {
                throw new ApplicationException($"Failed to attach to app session (expected).{Environment.NewLine}{exception}");
            }

            if (WindowsDriver == null)
                throw new ApplicationException($"Appium windows driver is null");

        }

        public static void TearDown()
        {
            if (WindowsDriver == null)
                return;

            foreach (var sessionWindowHandle in WindowsDriver.WindowHandles)
            {
                WindowsDriver.SwitchTo().Window(sessionWindowHandle);
                WindowsDriver.CloseApp();
            }

            WindowsDriver.Quit();
            WindowsDriver.Dispose();
            WindowsDriver = null;
        }
    }
}
