﻿using System;
using WindowsApplicationForAppium.AcceptanceTests.Contracts;

namespace WindowsApplicationForAppium.AcceptanceTests.Technicals
{
    public class AppiumGuiManager : IAppiumGuiManager
    {
        private readonly Lazy<IMainForm> mainForm;

        public IMainForm MainForm => mainForm.Value;

        public AppiumGuiManager()
        {
            mainForm = new Lazy<IMainForm>(() => new MainForm());
        }

        public void Dispose()
        {
            MainForm.Dispose();
        }
    }
}