﻿using System;
using WindowsApplicationForAppium.AcceptanceTests.Contracts;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Windows;

namespace WindowsApplicationForAppium.AcceptanceTests.Technicals
{
    internal class MainForm : IMainForm
    {
        private const string frmAppiumTestAutomationId = "frmAppiumTestSample";
        private const string clickButtonAutomationId = "btnClickMe";
        private const string labelAutomationId = "lblClickReport";

        private Lazy<WindowsElement> mainFormWindowElement;
        private Lazy<AppiumWebElement> clickButton;
        private Lazy<AppiumWebElement> label;

        private WindowsElement MainFormWindowElement => mainFormWindowElement.Value;
        private AppiumWebElement ClickButton => clickButton.Value;
        private AppiumWebElement Label => label.Value;

        public string LabelText => Label.Text;


        public MainForm()
        {
            mainFormWindowElement = new Lazy<WindowsElement>(() =>
            {
                WidowsDriverConfiguration.Setup();
                return WidowsDriverConfiguration.WindowsDriver.FindElementByAccessibilityId(frmAppiumTestAutomationId);
            });

            clickButton = new Lazy<AppiumWebElement>(() => MainFormWindowElement.FindElementByAccessibilityId(clickButtonAutomationId));

            label = new Lazy<AppiumWebElement>(() => MainFormWindowElement.FindElementByAccessibilityId(labelAutomationId));
        }

        public bool Run()
        {
            return MainFormWindowElement != null;
        }

        public void OnClick_ClickButton()
        {
            ClickButton.Click();
        }

        public void Dispose()
        {
            WidowsDriverConfiguration.TearDown();
        }
    }
}
