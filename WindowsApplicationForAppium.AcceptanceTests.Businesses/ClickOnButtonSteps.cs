﻿using WindowsApplicationForAppium.AcceptanceTests.Contracts;
using WindowsApplicationForAppium.AcceptanceTests.Flows;
using FluentAssertions;
using TechTalk.SpecFlow;

namespace WindowsApplicationForAppium.AcceptanceTests.Businesses
{
    [Binding]
    public class ClickOnButtonSteps
    {
        private readonly User user;

        public ClickOnButtonSteps(IAppiumGuiManager appiumGuiManager)
        {
            user = new User(appiumGuiManager.MainForm);
        }

        [Given(@"I have a WindowsApplicationForAppium From")]
        public void GivenIHaveAWindowsApplicationForAppiumFrom()
        {
            user.RunWindowsApplicationForm().Should().BeTrue();
        }

        [When(@"I click on click button")]
        public void WhenIClickOnClickButton()
        {
            user.ClickOnClickButton();
        }

        [Then(@"I can see '(.*)' on the label")]
        public void ThenICanSeeTextOnTheLabel(string expectedLabelText)
        {
            user.CanSeeExpectedTextOnLabel(expectedLabelText);
        }
    }
}
