﻿using WindowsApplicationForAppium.AcceptanceTests.Contracts;
using WindowsApplicationForAppium.AcceptanceTests.Technicals;
using BoDi;
using TechTalk.SpecFlow;

namespace WindowsApplicationForAppium.AcceptanceTests.Businesses
{
    [Binding]
    public class TestHook
    {
        private IObjectContainer objectContainer;

        public TestHook(IObjectContainer objectContainer)
        {
            this.objectContainer = objectContainer;
        }

        [BeforeScenario]
        public void InitializeWebDriver()
        {
            objectContainer.RegisterInstanceAs<IAppiumGuiManager>(new AppiumGuiManager());
        }

        [AfterScenario]
        public void DisposeWebDriver(IAppiumGuiManager appiumGuiManager)
        {
           appiumGuiManager.Dispose();
        }
    }
}
