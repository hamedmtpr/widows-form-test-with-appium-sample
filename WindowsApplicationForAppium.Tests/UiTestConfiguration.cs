﻿using System;
using System.IO;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Windows;
using Xunit;

namespace WindowsApplicationForAppium.Tests
{
    public class UiTestConfiguration
    {
        private const string WindowsApplicationDriverUrl = "http://127.0.0.1:4723";
        private const string appExecutableFilePath = @"..\..\..\WindowsApplicationForAppium\bin\Debug\WindowsApplicationForAppium.exe";

        public static WindowsDriver<WindowsElement> Session;

        public static void Setup()
        {
            Assert.True(File.Exists(appExecutableFilePath));

            var appExecutableFileFullName = new FileInfo(appExecutableFilePath).FullName;

            if (Session == null)
            {
                var appiumOptions = new AppiumOptions();
                appiumOptions.AddAdditionalCapability("app", appExecutableFileFullName);
                appiumOptions.AddAdditionalCapability("deviceName", "WindowsPC");
                WindowsDriver<WindowsElement> DesktopSession = null;
                try
                {
                    Console.WriteLine("Trying to Launch App");
                    DesktopSession = new WindowsDriver<WindowsElement>(new Uri(WindowsApplicationDriverUrl), appiumOptions);
                }
                catch(Exception e)
                {
                    Console.WriteLine("Failed to attach to app session (expected).");
                }

                appiumOptions.AddAdditionalCapability("app", "Root");
                DesktopSession = new WindowsDriver<WindowsElement>(new Uri(WindowsApplicationDriverUrl), appiumOptions);
                var mainWindow = DesktopSession.FindElementByAccessibilityId("frmAppiumTestSample");
                Console.WriteLine("Getting Window Handle");
                var mainWindowHandle = mainWindow.GetAttribute("NativeWindowHandle");
                mainWindowHandle = (int.Parse(mainWindowHandle)).ToString("x"); // Convert to Hex
                appiumOptions = new AppiumOptions();
                appiumOptions.AddAdditionalCapability("appTopLevelWindow", mainWindowHandle);
                Session = new WindowsDriver<WindowsElement>(new Uri(WindowsApplicationDriverUrl), appiumOptions);

                //--------------
                //DesiredCapabilities appCapabilities = new DesiredCapabilities();
                //appCapabilities.SetCapability("app", appExecutableFilePath);
                //appCapabilities.SetCapability("deviceName", "WindowsPC");



                //Uri dd = new Uri(WindowsApplicationDriverUrl);
                //Session = new WindowsDriver<WindowsElement>(dd, appCapabilities);
                Assert.NotNull(Session);

                Session.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(1.5);
            }
        }

        public static void TearDown()
        {
            if (Session != null)
            {
                foreach (var sessionWindowHandle in Session.WindowHandles)
                {
                    Session.SwitchTo().Window(sessionWindowHandle);
                    Session.CloseApp();
                }
                Session.Quit();
                Session.Dispose();
                Session = null;
            }
        }
    }

}
