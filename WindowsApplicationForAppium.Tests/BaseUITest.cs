﻿using System;
using Xunit;

namespace WindowsApplicationForAppium.Tests
{
    [Trait("Category", "AcceptanceTest")]
    public class BaseUiTest : IDisposable
    {
        public BaseUiTest()
        {
            UiTestConfiguration.Setup();
        }

        public void Dispose()
        {
            UiTestConfiguration.TearDown();
        }
    }
}