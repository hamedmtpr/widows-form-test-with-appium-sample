﻿using Xunit;

namespace WindowsApplicationForAppium.Tests
{
    public class UiTest : BaseUiTest
    {
        [Fact]
        public void Click_On_btnClickMe_Should_Change_lblClickReport_Text()
        {
            // Arrange
            var frmAppiumTestSampleId = "frmAppiumTestSample";
            var btnClickMeId= "btnClickMe";
            var lblClickReportId = "lblClickReport";

            var currentWindowElement = UiTestConfiguration.Session.FindElementByAccessibilityId(frmAppiumTestSampleId);
            var buttonTestElement = currentWindowElement.FindElementByAccessibilityId(btnClickMeId);
            var labelElement = currentWindowElement.FindElementByAccessibilityId(lblClickReportId);

            var expected = "Clicked!";

            // Act
            buttonTestElement.Click();
            var actual = labelElement.Text;

            // Assert
            Assert.Equal(actual, expected);
        }
    }
}
