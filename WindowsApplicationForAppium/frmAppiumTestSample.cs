﻿using System;
using System.Windows.Forms;

namespace WindowsApplicationForAppium
{
    public partial class frmAppiumTestSample : Form
    {
        public frmAppiumTestSample()
        {
            InitializeComponent();
        }

        private void btnClickMe_Click(object sender, EventArgs e)
        {
            lblClickReport.Text = "Clicked!";
        }
    }
}
