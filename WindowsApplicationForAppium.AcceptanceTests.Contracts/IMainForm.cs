﻿using System;

namespace WindowsApplicationForAppium.AcceptanceTests.Contracts
{
    public interface IMainForm : IDisposable
    {
        string LabelText { get; }
        bool Run();
        void OnClick_ClickButton();
    }
}
