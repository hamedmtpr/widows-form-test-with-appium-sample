﻿using System;

namespace WindowsApplicationForAppium.AcceptanceTests.Contracts
{
    public interface IAppiumGuiManager : IDisposable
    {
        IMainForm MainForm { get; }
    }
}