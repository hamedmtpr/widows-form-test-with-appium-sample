﻿using WindowsApplicationForAppium.AcceptanceTests.Contracts;

namespace WindowsApplicationForAppium.AcceptanceTests.Flows
{
    public class User
    {
        private readonly IMainForm mainForm;

        public User(IMainForm mainForm)
        {
            this.mainForm = mainForm;
        }

        public bool RunWindowsApplicationForm()
        {
            return mainForm.Run();
        }

        public void ClickOnClickButton()
        {
            mainForm.OnClick_ClickButton();
        }

        public bool CanSeeExpectedTextOnLabel(string expectedLabelText)
        {
            return expectedLabelText.Equals(mainForm.LabelText);
        }
    }
}
